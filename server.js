// database configuration, error handling stuff, environment variables
const dotenv = require('dotenv');
dotenv.config({ path: `./config.env` });
const mongoose = require('mongoose');
const AppError = require('./utils/appError');
AppError.unhandledRejection();
AppError.uncaughtException();

// 1 Connect to mongoose
mongoose
  .connect(process.env.MONGODB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(con => console.log('Connected to database'));

// 2 Include app
const app = require('./app');

// 3 Run app on port 3000
const port = process.env.port || 3000;
app.listen(port, () => {
  console.log(`app runing on port ${port}`);
});
