class appError extends Error {
  constructor(message, statusCode) {
    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
    this.isOperational = true;
    Error.captureStackTrace(this, this.constructor);
  }

  static unhandledRejection() {
    process.on('unhandledRejection', err => {
      console.log(`${err.name}: ${err.message}: ${err.stack}`);
      // process.exit(1);
    });
  }

  static uncaughtException() {
    process.on('uncaughtException', err => {
      console.log(`${err.name}: ${err.message}: ${err.stack}`);
      // process.exit(1);
    });
  }

  static globalErrorHandler(err, req, res, next) {
    console.log(err.stack);

    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error';

    if (process.env.ENVIRONMENT == 'development') {
      if (req.originalUrl.startsWith('/api')) {
        return res.status(err.statusCode).json({
          status: err.status,
          error: err,
          message: err.message,
          stack: err.stack
        });
      } else {
        console.error('ERROR', err);
        return res.status(err.statusCode).render('error', {
          title: 'Something went wrong',
          msg: err.message
        });
      }
    } else if (process.env.ENVIRONMENT == 'production') {
      // Operational trusted errors
      let error = { ...err };
      error.message = err.message;

      if (err.name == 'CastError') err = appError.handleCastErrorDB(error);
      if (err.code == 11000) err = appError.handleDuplicateFieldsDB(error);
      if (err.name == 'ValidationError')
        err = appError.handleValidationErrorDB(error);
      if (err.name == 'JsonWebTokenError') err = handleJWTError(err);

      if (req.originalUrl.startsWith('/api')) {
        if (err.isOperational) {
          return res.status(err.statusCode).json({
            status: err.status,
            message: err.message
          });

          // Programing or other unknown error: don't leak error details
        } else {
          console.error('ERROR', err);
          return res.status(err.statusCode).json({
            status: 'error',
            message: 'Something went wrong!'
          });
        }
      } else {
        if (err.isOperational) {
          return res.status(err.statusCode).render('error', {
            title: 'Something went wrong',
            msg: err.message
          });

          // Programing or other unknown error: don't leak error details
        } else {
          console.error('ERROR', err);

          return res.status(err.statusCode).render('error', {
            title: 'Something went wrong',
            msg: 'Please try again latter'
          });
        }
      }
    }
  }

  static handleJWTError(err) {
    const message = `Invalid token please login again`;
    return new appError(message, 401);
  }

  static handleCastErrorDB(err) {
    const message = `Invalid ${err.path}: ${err.value}`;
    return new appError(message, 400);
  }

  static handleDuplicateFieldsDB(err) {
    const message = `Duplicate field value`;
    return new appError(message, 400);
  }

  static handleValidationErrorDB(err) {
    const errors = Object.values(err.errors).map(el => el.message);
    const message = `Invalid input data. ${errors.join('. ')}`;
    return new appError(message, 400);
  }

  static catchAsync(fn) {
    return (req, res, next) => {
      fn(req, res, next).catch(err => next(err));
    };
  }
}

module.exports = appError;
